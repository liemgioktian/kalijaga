<?php

Class simulasi extends my_model {

  var $table = 'jatahdesa';
  var $theads = array (
    array('jamaah', 'NAMA LENGKAP'),
    array('bulan', 'BULAN'),
    array('item', 'ITEM INFAQ'),
    array('dihitung', 'DIBAGI'),
    array('dibulatkan', 'DIBULATKAN'),
  );
  var $filters = array (
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah.id'
    ),
    array (
      'label' => 'BULAN',
      'name' => 'tanggalbulan'
    ),
  );
  var $tfoots = array (
    'a' => '',
    'b' => '',
    'c' => '',
    'd' => '',
    'totaldijatah' => 0,
    'totalpembulatan' => 0,
    'e' => '',
  );

  function __construct () {
    parent::__construct();
    $jamaah = $this->db->get('jamaah')->result();
    $this->filters[0]['options'][] = array('value' => '', 'text' => '');
    foreach ($jamaah as $j) $this->filters[0]['options'][] = array('value' => $j->id, 'text' => $j->nama);
  }

  function find($where = array()) {
    if (isset($where->tanggalbulan)) $this->tanggalbulan_to_bulantahun($where);
    $this->db->select('jatahdesa.*');
    $this->db->select("CONCAT(MONTHNAME(STR_TO_DATE(bulan, '%m')), ' ', tahun) as bulan", false);

    $this->db->select('jamaah.nama as jamaah', false);
    $this->db->join('jamaah', true, false);

    $this->db->select('infaq.item as item', false);
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');

    $this->db->select("CONCAT('Rp ', FORMAT(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal, 0)) as dihitung", false);
    $this->db->select("CONCAT('Rp ',FORMAT( round(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal / 1000, 0) * 1000, 0)) as dibulatkan", false);
    $this->db->select("rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal as totaldijatah", false);
    $this->db->select("round(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal / 1000, 0) * 1000 as totalpembulatan", false);
    $this->db->join('rumus', 'jamaah.ring = rumus.ring');
    // parent::find($where); die($this->db->last_query());
    return parent::find($where);
  }
}
