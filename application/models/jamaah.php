<?php

Class jamaah extends my_model {

  var $table = 'jamaah';
  var $theads = array (
    array('nama', 'NAMA LENGKAP'),
    array('ring', 'KEMAMPUAN'),
    array('kk', 'KELUARGA'),
  );
  var $fields = array (
    array (
      'label' => 'NAMA LENGKAP',
      'name' => 'nama'
    ),
    array (
      'label' => 'KEMAMPUAN',
      'name' => 'ring'
    ),
    array (
      'label' => 'KELUARGA',
      'name' => 'kk'
    ),
  );

  function __construct () {
    parent::__construct();
    $this->fields[2]['options'] = array (
      array (
        'value' => '0',
        'text' => 'ANGGOTA KELUARGA'
      ),
      array (
        'value' => '1',
        'text' => 'KEPALA KELUARGA'
      ),
    );
    $this->fields[1]['options'] = array (
      array (
        'value' => '0',
        'text' => 'TIDAK MEMILIKI KEWAJIBAN INFAQ'
      ),      array (
        'value' => '1',
        'text' => 'AGHNIYA'
      ),
      array (
        'value' => '2',
        'text' => 'MAMPU'
      ),
      array (
        'value' => '3',
        'text' => 'MENENGAH'
      ),
      array (
        'value' => '4',
        'text' => 'DHUAFA'
      ),
    );
  }

  function find ($where = array()) {
    $this->db->select('jamaah.*');
    $this->db->select("IF(kk=1, 'KEPALA KELUARGA', '') as kk", false);
    $this->db->select("CASE ring ".
    "WHEN '1' THEN 'AGHNIYA' ".
    "WHEN '2' THEN 'MAMPU' ".
    "WHEN '3' THEN 'MENENGAH' ".
    "WHEN '4' THEN 'DHUAFA' ".
    "ELSE 'UNKNOWN' END as ring", false);
    return parent::find($where);
  }

}
