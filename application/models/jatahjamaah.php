<?php

Class jatahjamaah extends my_model {

  var $table = 'jatahjamaah';
  var $theads = array (
    array('jamaah', 'NAMA LENGKAP'),
    array('bulan', 'BULAN'),
    array('item', 'ITEM INFAQ'),
    array('jatah_format', 'JATAH'),
    array('setor_format', 'DITERIMA'),
    // array('tanggalsetor', 'TANGGAL'),
  );
  var $fields = array(
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggalsetor'
    ),
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah_alias'
    ),
    array (
      'label' => 'INFAQ',
      'name' => 'jatahdesa_alias'
    ),
    array (
      'label' => 'JATAH',
      'name' => 'dibulatkan_alias'
    ),
    array (
      'label' => 'INFAQ MASUK',
      'name' => 'disetorkan'
    ),
  );
  var $filters = array (
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah.id'
    ),
    array (
      'label' => 'ITEM INFAQ',
      'name' => 'infaq.id'
    ),
    array (
      'label' => 'BULAN',
      'name' => 'tanggalbulan'
    ),
  );
  var $tfoots = array (
    'a' => '',
    'b' => '',
    'c' => '',
    'd' => '',
    'dijatah' => 0,
    'disetorkan' => 0,
    // 'e' => '',
    'f' => ''
  );

  function __construct () {
    parent::__construct();
    $jamaah = $this->db->order_by('nama', 'asc')->get('jamaah')->result();
    $this->filters[0]['options'][] = array('value' => '', 'text' => '');
    foreach ($jamaah as $j) $this->filters[0]['options'][] = array('value' => $j->id, 'text' => $j->nama);
    $infaq = $this->db->get('infaq')->result();
    $this->filters[1]['options'][] = array('value' => '', 'text' => '');
    foreach ($infaq as $j) $this->filters[1]['options'][] = array('value' => $j->id, 'text' => $j->item);
  }

  function findOne ($id) {
    $this->db->join('jatahdesa', 'jatahdesa.id = jatahjamaah.jatahdesa');
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');
    $this->db->join('jamaah', 'jatahjamaah.jamaah = jamaah.id');
    $this->db->select('jamaah.nama as jamaah_alias', false);
    $this->db->select("CONCAT('Rp ', FORMAT( dibulatkan, 0)) as dibulatkan_alias", false);
    $this->db->select('jatahjamaah.*');
    $this->db->select("CONCAT(infaq.item, ' ', MONTHNAME(STR_TO_DATE(bulan, '%m')), ' ', tahun) as jatahdesa_alias", false);

    $this->db->select('dibulatkan as disetorkan', false);
    // $this->db->select("'2017-05-06' as tanggalsetor", false);

    return parent::findOne($id);
  }

  function find($where = array()) {
    if (isset($where->tanggalbulan)) $this->tanggalbulan_to_bulantahun($where);
    $this->db->select('jatahjamaah.*');
    $this->db->select("DATE_FORMAT(tanggalsetor,'%d %b %Y') as tanggalsetor", false);
    $this->db->select("CONCAT('RP ', FORMAT(IFNULL(dibulatkan, 0) ,0)) as jatah_format", false);
    $this->db->select("CONCAT('RP ', FORMAT(IFNULL(disetorkan, 0) ,0)) as setor_format", false);

    $this->db->select('jamaah.nama as jamaah', false);
    $this->db->join('jamaah', 'jatahjamaah.jamaah = jamaah.id', 'right');

    $this->db->select("CONCAT(MONTHNAME(STR_TO_DATE(jatahdesa.bulan, '%m')), ' ', tahun) as bulan", false);
    $this->db->join('jatahdesa', 'jatahjamaah.jatahdesa = jatahdesa.id', false);

    $this->db->select('infaq.item as item', false);
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');

    $this->db->join('rumus', 'jamaah.ring = rumus.ring');
    // parent::find($where); die($this->db->last_query());
    return parent::find($where);
  }

  function setor ($id) {
    $this->db->query("UPDATE jatahjamaah SET disetorkan = dibulatkan, tanggalsetor = DATE(NOW()) WHERE id = " . $id);
  }
}
