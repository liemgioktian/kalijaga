<?php

Class kaskelompok extends my_model {

  var $table = 'kaskelompok';
  var $theads = array (
    array('tanggalformat', 'TANGGAL'),
    array('deskripsi', 'DESKRIPSI'),
    // array('qty', 'QTY'),
    array('debet_', 'MASUK'),
    array('kredit_', 'KELUAR'),
    array('saldo', 'SALDO'),
    array('notes', 'CATATAN'),
  );
  var $fields = array (
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggal'
    ),
    array (
      'label' => 'DESKRIPSI',
      'name' => 'deskripsi'
    ),
    array (
      'label' => 'QTY',
      'name' => 'qty'
    ),
    array (
      'label' => 'MASUK',
      'name' => 'debet'
    ),
    array (
      'label' => 'KELUAR',
      'name' => 'kredit'
    ),
    array (
      'label' => 'CATATAN',
      'name' => 'notes'
    ),
  );
  // var $tfoots = array (
  //   'a' => '',
  //   'b' => '',
  //   'c' => '',
  //   'd' => '',
  //   'debet' => 0,
  //   'kredit' => 0,
  //   'e' => '',
  //   'f' => '',
  //   'g' => ''
  // );

  function __construct () {
    parent::__construct();
  }

  function find ($where = array()) {
    $this->db->select($this->table . '.*');
    $this->db->select("DATE_FORMAT(tanggal,'%d %b %Y') as tanggalformat", false);
    $this->db->select("CONCAT('RP ', FORMAT(IFNULL(debet, 0) ,0)) as debet_", false);
    $this->db->select("CONCAT('RP ', FORMAT(IFNULL(kredit, 0) ,0)) as kredit_", false);
    $this->db->order_by('tanggal');
    $this->db->order_by('id');
    // parent::find($where);die($this->db->last_query());
    $records = parent::find($where);
    $saldo = 0;
    foreach ($records as &$record) {
      $saldo += $record->debet;
      $saldo -= $record->kredit;
      $record->saldo = "Rp " . number_format($saldo,2,',','.');
    }
    return $records;
  }

}
