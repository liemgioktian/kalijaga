<?php

Class jatahdesa extends my_model {

  var $table = 'jatahdesa';
  var $theads = array (
    array('bulan', 'BULAN'),
    array('item', 'ITEM INFAQ'),
    array('formatted_nominal', 'JATAH'),
    array('terpenuhi', 'SUDAH MASUK'),
    array('kekurangan', 'TOMBOK'),
    array('kelebihan', 'BATHI'),
  );
  var $tfoots = array (
    'a' => '',
    'b' => '',
    'c' => '',
    'nominal' => 0,
    'sudahmasuk' => 0,
    'totalkekurangan' => 0,
    'totalkelebihan' => 0,
    'd' => '',
  );
  var $fields = array (
    array (
      'label' => 'BULAN / TAHUN',
      'name' => 'tanggalbulan'
    ),
    array (
      'label' => 'ITEM INFAQ',
      'name' => 'item'
    ),
    array (
      'label' => 'TOTAL',
      'name' => 'nominal'
    ),
  );
  var $filters = array (
    array (
      'label' => 'ITEM INFAQ',
      'name' => 'item'
    ),
    array (
      'label' => 'BULAN',
      'name' => 'tanggalbulan'
    ),
    array (
      'label' => 'LEVEL',
      'name' => 'level'
    ),
  );

  function __construct () {
    parent::__construct();
    $items = $this->db->get('infaq')->result();
    $options = array();
    foreach ($items as $item) 
      $options[] = array('value' => $item->id, 'text' => "$item->item ($item->level)");
    $this->fields[1]['options'] = $options;
    array_unshift($options, array('value' => '', 'text' => ''));
    $this->filters[0]['options'] = $options;
  }

  function find($where = array()) {
    if (isset($where->tanggalbulan)) $this->tanggalbulan_to_bulantahun($where);

    $this->db->select('jatahdesa.*');
    $this->db->select('infaq.item as item');
    $this->db->select("CONCAT('Rp ', FORMAT( nominal, 0)) as formatted_nominal", false);
    $this->db->select("CONCAT(MONTHNAME(STR_TO_DATE(jatahdesa.bulan, '%m')), ' ', tahun) as bulan", false);
    $this->db->join('infaq', 'infaq.id = jatahdesa.item');

    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(SUM(disetorkan), 0), 0)) as terpenuhi", false);
    $this->db->select("IF(0=nominal, 0, IFNULL(SUM(disetorkan), 0)) as sudahmasuk", false);

    $this->db->select("CONCAT('Rp ', FORMAT(IF(nominal >= SUM(disetorkan), nominal - SUM(disetorkan), 0), 0)) as kekurangan", false);
    $this->db->select("IF(nominal >= SUM(disetorkan), nominal - SUM(disetorkan), 0) as totalkekurangan", false);
    $this->db->select("CONCAT('Rp ', FORMAT(IF(nominal <= SUM(disetorkan) AND nominal > 0, SUM(disetorkan) - nominal, 0), 0)) as kelebihan", false);
    $this->db->select("IF(nominal <= SUM(disetorkan) AND nominal > 0, SUM(disetorkan) - nominal, 0) as totalkelebihan", false);

    $this->db->join('jatahjamaah', 'jatahjamaah.jatahdesa = jatahdesa.id','left');
    $this->db->group_by('jatahdesa.id');
// parent::find($where);die($this->db->last_query());
    return parent::find($where);
  }

  function save ($data, $id = null) {
    $split = explode('-', $data['tanggalbulan']);
    unset ($data['tanggalbulan']);
    $data['tahun'] = $split[0];
    $data['bulan'] = $split[1];
    return parent::save ($data);
  }
}
