<?php

Class infaq extends my_model {

  var $table = 'infaq';
  var $theads = array (
    array('item', 'ITEM INFAQ'),
    array('level', 'LEVEL'),
  );
  var $fields = array (
    array (
      'label' => 'ITEM INFAQ',
      'name' => 'item'
    ),
    array (
      'label' => 'LEVEL',
      'name' => 'level'
    ),
  );
  var $filters = array (
    array (
      'label' => 'LEVEL',
      'name' => 'level'
    ),
  );

  function __construct () {
    parent::__construct();

    $this->filters[0]['options'][] = array ('value' => '', 'text' => 'SEMUA');
    $this->filters[0]['options'] = $this->fields[1]['options'] = array(
      array (
        'value' => 'PUSAT',
        'text' => 'PUSAT'
      ),
      array (
        'value' => 'DAERAH',
        'text' => 'DAERAH'
      ),
      array (
        'value' => 'DESA',
        'text' => 'DESA'
      ),
      array (
        'value' => 'KELOMPOK',
        'text' => 'KELOMPOK'
      ),
    );
  }

}
