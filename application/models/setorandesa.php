<?php

Class setorandesa extends my_model {

  var $table = 'setorandesa';
  var $theads = array (
    array('tanggal', 'PER TANGGAL'),
    array('jatahdesa', 'JATAH INFAQ'),
    array('jatah', 'JATAH'),
    array('diterima', 'TOTAL DITERIMA'),
    array('disetor', 'TOTAL DISETOR'),
    array('belumdisetor', 'BELUM DISETOR'),
    array('harusdisetor', 'HARUS DISETOR'),
    array('masukkas', 'MASUK KAS'),
  );
  var $fields = array (
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggal'
    ),
    array (
      'label' => 'JATAH',
      'name' => 'jatahdesa'
    ),
    array (
      'label' => 'NOMINAL',
      'name' => 'nominal'
    ),
  );
  var $filters = array (
    array (
      'label' => 'JATAH BULAN',
      'name' => 'tanggalbulan'
    ),
  );

  function __construct () {
    parent::__construct();
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');
    $this->db->select('jatahdesa.*');
    $this->db->select('infaq.item as item', false);
    $this->db->select("CONCAT(MONTHNAME(STR_TO_DATE(bulan, '%m')), ' ', tahun) as bulan", false);
    $jatahdesa = $this->db->get('jatahdesa')->result();
    $options = array();
    foreach ($jatahdesa as $jatah) $options[] = array(
      'value' => $jatah->id,
      'text' => "$jatah->item $jatah->bulan",
    );
    $this->fields[1]['options'] = $options;
  }

  function find ($where = array()) {
    if (isset($where->tanggalbulan)) $this->tanggalbulan_to_bulantahun($where);
    $this->db->select("$this->table.*");
    $this->db->select("DATE_FORMAT(tanggal,'%d %b %Y') as tanggal", false);

    $this->db->join('jatahdesa', 'jatahdesa.id = setorandesa.jatahdesa');
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');
    $this->db->select("CONCAT(infaq.item, ' ', MONTHNAME(STR_TO_DATE(bulan, '%m')), ' ', tahun) as jatahdesa", false);

    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(jatahdesa.nominal, 0) ,0)) as jatah", false);
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL((select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal), 0) ,0)) as disetor", false);

    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(IF((select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal) <= jatahdesa.nominal, SUM(jatahjamaah.disetorkan) - (select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal), 0), 0) ,0)) as belumdisetor", false);
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(IF(jatahdesa.nominal > 0 AND (select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal) >= jatahdesa.nominal, SUM(jatahjamaah.disetorkan) - (select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal), 0), 0) ,0)) as masukkas", false);
    $this->db->select("CONCAT('Rp ', FORMAT(IF(jatahdesa.nominal > 0, IFNULL(jatahdesa.nominal - (select sum(child.nominal) from setorandesa child where child.jatahdesa = jatahdesa.id and child.tanggal <= setorandesa.tanggal), 0), 0), 0)) as harusdisetor", false);

    $this->db->join('jatahjamaah', 'jatahjamaah.jatahdesa = jatahdesa.id AND setorandesa.tanggal >= jatahjamaah.tanggalsetor');
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(SUM(jatahjamaah.disetorkan), 0) ,0)) as diterima", false);
    $this->db->group_by("$this->table.id");

    $this->db->order_by('tanggal');
    // parent::find($where); die($this->db->last_query());
    return parent::find($where);
  }

}
