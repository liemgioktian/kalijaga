<?php

Class motor extends my_model {

  var $table = 'motor';
  var $theads = array (
    array('tanggalformat', 'TANGGAL'),
    array('jamaah', 'JAMAAH'),
    array('nominalformat', 'NOMINAL'),
  );
  var $fields = array (
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggal'
    ),
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah'
    ),
    array (
      'label' => 'NOMINAL',
      'name' => 'nominal'
    ),
  );
  var $tfoots = array (
    'a' => '',
    'b' => '',
    'c' => '',
    'nominal' => 0,
    'd' => '',
  );
  var $filters = array (
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah.id'
    ),
  );
  function __construct () {
    parent::__construct();
    $jamaah = $this->db->get('jamaah')->result();
    $options= array();
    foreach ($jamaah as $j) $options[] = array ('value' => $j->id, 'text' => $j->nama);
    $this->fields[1]['options'] = $options;
    $this->filters[0]['options']= $options;
  }

  function find ($where = array()) {
    $this->db->select($this->table . '.*');
    $this->db->select('jamaah.nama as jamaah');
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(nominal, 0) ,0)) as nominalformat", false);
    $this->db->select("DATE_FORMAT(tanggal,'%d %b %Y') as tanggalformat", false);
    $this->db->join('jamaah', 'jamaah.id = motor.jamaah');
    $this->db->order_by('tanggal');
    return parent::find($where);
  }

}
