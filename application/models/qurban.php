<?php

Class qurban extends my_model {

  var $table = 'qurban';
  var $theads = array (
    array('tanggalformat', 'TANGGAL'),
    array('jamaah', 'JAMAAH'),
    array('nominalformat', 'NOMINAL'),
  );
  var $fields = array (
    array (
      'label' => 'TANGGAL',
      'name' => 'tanggal'
    ),
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah'
    ),
    array (
      'label' => 'NOMINAL',
      'name' => 'nominal'
    ),
  );
  var $tfoots = array (
    'a' => '',
    'b' => '',
    'c' => '',
    'nominal' => 0,
    'd' => '',
  );
  var $filters = array (
    array (
      'label' => 'JAMAAH',
      'name' => 'jamaah.id'
    ),
    array (
      'label' => 'BULAN',
      'name' => 'tanggalbulan'
    )
  );
  function __construct () {
    parent::__construct();
    $jamaah = $this->db->get('jamaah')->result();
    $options= array(array ('value' => '', 'text' => ''));
    foreach ($jamaah as $j) $options[] = array ('value' => $j->id, 'text' => $j->nama);
    $this->fields[1]['options'] = $options;
    $this->filters[0]['options']= $options;
  }

  function find ($where = array()) {
    if (isset($where->tanggalbulan)) {
      $timestamp = strtotime($where->tanggalbulan);
      $php_date = getdate($timestamp);
      $bulan = date("n", $timestamp);
      $tahun = date("Y", $timestamp);
      $this->db->where('MONTH(tanggal)', $bulan, false);
      $this->db->where('YEAR(tanggal)', $tahun, false);
      unset($where->tanggalbulan);
    }
    $this->db->select($this->table . '.*');
    $this->db->select('jamaah.nama as jamaah');
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(nominal, 0) ,0)) as nominalformat", false);
    $this->db->select("DATE_FORMAT(tanggal,'%d %b %Y') as tanggalformat", false);
    $this->db->join('jamaah', 'jamaah.id = qurban.jamaah');
    $this->db->order_by('tanggal');
    return parent::find($where);
  }

}
