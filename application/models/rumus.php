<?php

Class rumus extends my_model {

  var $table = 'rumus';
  var $theads = array (
    array('ring', 'KEMAMPUAN'),
    array('prosentase', 'PROSENTASE'),
    array('jamaah', 'JUMLAH JAMAAH'),
    array('masing', 'MASING-MASING'),
  );
  var $fields = array (
    array (
      'label' => 'KEMAMPUAN',
      'name' => 'ring'
    ),
    array (
      'label' => 'PROSENTASE',
      'name' => 'prosentase'
    ),
  );

  function __construct () {
    parent::__construct();
    $this->fields[0]['options'] = array (
      array (
        'value' => '1',
        'text' => 'AGHNIYA'
      ),
      array (
        'value' => '2',
        'text' => 'MAMPU'
      ),
      array (
        'value' => '3',
        'text' => 'MENENGAH'
      ),
      array (
        'value' => '4',
        'text' => 'DHUAFA'
      ),
    );
  }

  function find ($where = array()) {
    $this->db->select('rumus.*');
    $this->db->select("CONCAT(rumus.prosentase, ' %') as prosentase", FALSE);
    $this->db->select("CONCAT(count(jamaah.id), ' Orang') as jamaah", false);
    $this->db->select("CONCAT(ROUND(rumus.prosentase / count(jamaah.id), 2), ' %') as masing", false);
    $this->db->join('jamaah', 'rumus.ring=jamaah.ring');
    $this->db->group_by('rumus.id');
    $this->db->select("CASE rumus.ring ".
    "WHEN '1' THEN 'AGHNIYA' ".
    "WHEN '2' THEN 'MAMPU' ".
    "WHEN '3' THEN 'MENENGAH' ".
    "WHEN '4' THEN 'DHUAFA' ".
    "ELSE 'UNKNOWN' END as ring", false);
    return $this->db->get($this->table)->result();
  }
}
