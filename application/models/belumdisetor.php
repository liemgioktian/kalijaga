<?php

Class belumdisetor extends my_model {

  var $table = 'jatahjamaah';
  var $theads = array (
    array('nama', 'NAMA LENGKAP'),
    array('infaq', 'INFAQ'),
    array('diterima', 'DITERIMA'),
    array('disetor', 'DISETOR'),
    array('belumdisetor', 'BELUM DISETOR'),
  );
  // var $tfoots = array (
  //   'a' => '',
  //   'b' => '',
  //   'c' => '',
  //   'd' => '',
  //   'debet' => 0,
  //   'kredit' => 0,
  //   'e' => '',
  //   'f' => '',
  //   'g' => ''
  // );

  function __construct () {
    parent::__construct();
  }

  function find ($where = array()) {
    $this->db->select("$this->table.*");
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(disetorkan, 0) ,0)) as diterima", false);
    $this->db->select('jamaah.nama');
    $this->db->join('jamaah', 'jatahjamaah.jamaah = jamaah.id');
    $this->db->select('jamaah.nama');
    $this->db->select("CONCAT(infaq.item, ' ', MONTHNAME(STR_TO_DATE(bulan, '%m')), ' ', tahun) as infaq", false);
    $this->db->join('jatahdesa', 'jatahjamaah.jatahdesa = jatahdesa.id');
    $this->db->join('infaq', 'jatahdesa.item = infaq.id');
    $this->db->select("CONCAT('Rp ', FORMAT(SUM(IFNULL(setorandesa.nominal, 0)) ,0)) as disetor", false);
    $this->db->select("CONCAT('Rp ', FORMAT(IFNULL(disetorkan, 0) - SUM(IFNULL(setorandesa.nominal, 0)) ,0)) as belumdisetor", false);
    $this->db->join('setorandesa', 'setorandesa.jatahdesa = jatahdesa.id', 'left');
    $this->db->group_by("$this->table.id");
    // parent::find($where);die($this->db->last_query());
    return parent::find($where);
  }

}
