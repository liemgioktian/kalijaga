  <script type="text/javascript">
    var site_url = '<?= site_url() ?>'
  </script>
  <?php foreach (array (
    'jquery-1.7.2.min.js',
    'bootstrap.js',
    'bootstrap-datepicker.js',
    'jquery.dataTables.min.js',
    'DT_bootstrap.js',
    'select2.min.js',
    'base.js',
  ) as $js): ?> <script type="text/javascript" src="<?= base_url("js/$js") ?>"></script> <?php endforeach ?>

  </body>

</html>