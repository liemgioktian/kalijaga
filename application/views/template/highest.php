<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Kelompok Kalijaga</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <?php foreach (array(
      'bootstrap.min.css',
      'bootstrap-responsive.min.css',
      'datepicker.css',
      'css.css',
      'font-awesome.css',
      'jquery.dataTables.min.css',
      'DT_bootstrap.css',
      'select2.min.css',
      'style.css',
      'henrisusanto.css'
    ) as $css): ?> <link href="<?= base_url("css/$css") ?>" rel="stylesheet"> <?php endforeach ?>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>