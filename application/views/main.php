<div class="main">
  
  <div class="main-inner">

      <div class="container">
  
        <div class="row">
          
          <div class="span12">          
            
            <div class="widget ">
              
            <div class="widget-header">
              <h3>Loading, Please Wait ... </h3>
            </div> <!-- /widget-header -->
          
          <div class="widget-content">
            
            
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="<?= !isset ($afterinsert) ? 'active' : '' ?>">
                  <a href="#formcontrols" data-toggle="tab">Data</a>
                </li>
                <li class="<?= isset ($afterinsert) ? 'active' : '' ?>">
                  <a href="#jscontrols" data-toggle="tab" class="<?= count($fields) < 1 ? 'hidden':'' ?>">Input</a>
                </li>
              </ul>
              <br>
              <div class="tab-content">
                <div class="tab-pane <?= !isset ($afterinsert) ? 'active' : '' ?>" id="formcontrols">
<!-- notif -->
                  <div class="alert alert-danger text-center hidden">
                    <div class="row">
                      <strong class="span2 offset3">Apakah Anda Yakin ?</strong>
                      <a class="btn btn-small btn-warning span1 tidak">Tidak</a>
                      <a class="btn btn-small btn-info span1 ya">Ya</a>
                    </div>
                  </div>
<!-- /notif -->
<!-- filter -->
                  <?php if (count ($filters) > 0): ?>
                  <form class="form-horizontal model-filter">
                    <fieldset>
                      <?php foreach ($filters as $field): ?>
                        <?php if (isset($field['options'])): ?>
                        <div class="control-group">                     
                          <label class="control-label"><?= $field['label'] ?></label>
                          <div class="controls">
                            <select class="span6" name="<?= $field['name'] ?>">
                              <?php foreach ($field['options'] as $option): ?>
                                <option value="<?= $option['value'] ?>"
                                <?= $option['value'] == $field['value'] ? 'selected':'' ?>
                                ><?= $option['text'] ?></option>
                              <?php endforeach ?>
                            </select>
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->
                        <?php else : ?>
                        <div class="control-group">                     
                          <label class="control-label"><?= $field['label'] ?></label>
                          <div class="controls">
                            <input type="text" class="span6" name="<?= $field['name'] ?>" value="<?= $field['value'] ?>">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->
                        <?php endif ?>
                      <?php endforeach ?>
                      
                      <div class="control-group">
                        <label></label>
                        <div class="controls">
                          <button type="submit" class="btn btn-primary">Filter</button> 
                          <a href="<?= current_url() ?>" class="btn btn-warning">Clear</a>
                        <div class="controls">
                      </div> <!-- /form-actions -->
                    </fieldset>
                  </form>
                <?php endif ?>
<!-- /filter -->
<!-- table -->
                  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>NO</th>
                        <?php foreach ($theads as $th): ?>
                          <th><?= $th[1] ?></th>
                        <?php endforeach ?>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=0; foreach ($items as $item): $no++; ?>
                        <tr>
                          <td><?= $no ?></td>
                          <?php foreach ($theads as $th): $field = $th[0] ?>
                            <td><?= $item->$field ?></td>
                          <?php endforeach ?>
                          <td>
                            <a class="btn btn-small btn-warning" href="<?= current_url() . '?mode=edit&id=' . $item->id ?>">detail</a>
                            <a class="btn btn-small btn-danger" href="<?= current_url() . '?mode=delete&id=' . $item->id ?>">hapus</a>
                          </td>
                        </tr>
                      <?php endforeach ?>
                    </tbody>
                    <tfoot>
                      <?php foreach ($tfoots as $field => $value): ?>
                      <th><?= strlen($value) > 0 ? $value : '' ?></th>
                      <?php endforeach ?>
                    </tfoot>
                  </table>                
<!-- /table -->
                </div>
                
                <div class="tab-pane <?= isset ($afterinsert) ? 'active' : '' ?>" id="jscontrols">
<!-- form -->
                <form class="form-horizontal" method="post">
                  <fieldset>
                    
                    <?php foreach ($fields as $field): ?>
                      <?php if (isset($field['options'])): ?>
                      <div class="control-group">                     
                        <label class="control-label"><?= $field['label'] ?></label>
                        <div class="controls">
                          <select class="span6" name="<?= $field['name'] ?>">
                            <?php foreach ($field['options'] as $option): ?>
                              <option value="<?= $option['value'] ?>"
                              <?= $option['value'] == $field['value'] ? 'selected':'' ?>
                              ><?= $option['text'] ?></option>
                            <?php endforeach ?>
                          </select>
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <?php else : ?>
                      <div class="control-group">                     
                        <label class="control-label"><?= $field['label'] ?></label>
                        <div class="controls">
                          <input type="text" class="span6" name="<?= $field['name'] ?>" value="<?= $field['value'] ?>">
                        </div> <!-- /controls -->       
                      </div> <!-- /control-group -->
                      <?php endif ?>
                    <?php endforeach ?>
                    
                     <br />
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary">Save</button> 
                      <a href="<?= current_url() ?>" class="btn btn-warning">Cancel</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
<!-- form -->
                </div>
              </div>
            </div>
            
            
            
            
            
          </div> <!-- /widget-content -->
            
        </div> <!-- /widget -->
            
        </div> <!-- /span8 -->
          
          
          
          
        </div> <!-- /row -->
  
      </div> <!-- /container -->
      
  </div> <!-- /main-inner -->
    
</div> <!-- /main -->