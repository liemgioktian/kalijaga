<?php

Class proses extends my_controller {


  function __construct () {
    parent::__construct();
  }

  function index () {
    $this->crud('jamaah');
  }

  function infaq () {
    $this->crud('infaq');
  }

  function rumus () {
    $this->crud('rumus');
  }

  function jatahdesa () {
    $this->crud('jatahdesa');
  }

  function simulasi () {
    $this->crud('simulasi');
  }

  function jatahjamaah () {
    $this->crud('jatahjamaah');
  }

  function kaskelompok () {
    $this->crud('kaskelompok');
  }

  function setorandesa () {
    $this->crud('setorandesa');
  }

  function belumdisetor () {
    $this->crud('belumdisetor');
  }

  function qurban () {
  	$this->crud('qurban');
  }

  function motor () {
    $this->crud('motor');
  }

  function setor ($id) {
    $this->load->model('jatahjamaah');
    $this->jatahjamaah->setor($id);
  }
}