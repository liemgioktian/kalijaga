<?php

Class my_controller extends CI_Controller {


  function __construct () {
    parent::__construct();
    $this->load->helper('url');
  }

  function loadview ($data) {

    $subnavbar['menus'] = array (
      array(site_url('proses'), 'group', 'Jamaah'),
      array(site_url('proses/infaq'), 'th-list', 'Daftar Infaq'),
      array(site_url('proses/jatahdesa'), 'download', 'Jatah'),
      array(site_url('proses/rumus'), 'cog', 'Rumus'),
      array(site_url('proses/simulasi'), 'dashboard', 'Simulasi'),
      array(site_url('proses/jatahjamaah'), 'edit', 'Kartu Infaq'),
      // array(site_url('proses/jatahjamaah'), 'check', 'Infaq Masuk'),
      // array(site_url('proses/belumdisetor'), 'fire', 'Belum Disetor'),
      array(site_url('proses/setorandesa'), 'upload', 'Setoran'),
      array(site_url('proses/kaskelompok'), 'shopping-cart', 'Kas Kelompok'),
      array(site_url('proses/qurban'), 'tint', 'Qurban'),
      // array(site_url('proses/motor'), 'road', 'Motor'),
    );

    $this->load->view('template/highest');
    $this->load->view('template/navbar');
    $this->load->view('template/subnavbar', $subnavbar);

    if (isset($data['viewer'])) $this->load->view($data['viewer'], $data['passparam']);
    else $this->load->view('main', $data['passparam']);

    $this->load->view('template/extra');
    $this->load->view('template/footer');
    $this->load->view('template/lowest');
  }  

  function crud ($model) {
    $get = $this->input->get();
    $post = $this->input->post();
    $data = array('passparam' => array());
    $this->load->model($model);

    if (isset($get['mode']) && $get['mode'] == 'delete' && $get['id']) {
      $this->$model->delete($get['id']);
      redirect (current_url());
    }

    if ($post && $get && $get['id']) {
      $this->$model->save($post, $get['id']);
      redirect (current_url());
    } else if ($post) $this->$model->save($post);

    $where = isset($get['where']) ? json_decode($get['where']) : new stdClass();
    $filters = clone ($where);

    $data['passparam']['items'] = $this->$model->find($where);
    $data['passparam']['theads'] = $this->$model->theads();
    $data['passparam']['filters'] = $this->$model->filters();
    $data['passparam']['tfoots'] = $this->$model->tfoots();

    foreach ($data['passparam']['filters'] as &$filter) {
      if (!is_object($filter)) $filter['value'] = '';
      else $filter['value'] = isset ($filters->$filter['name']) ? $filters->$filter['name'] : '';
    }

    $fieldsToRp = array();
    if (count ($data['passparam']['tfoots']) > 0) {
      foreach ($data['passparam']['items'] as $record) {
        foreach ($data['passparam']['tfoots'] as $field => &$value) {
          if (isset($record->$field)) {
            $value += $record->$field;
            if (!in_array($field, $fieldsToRp)) $fieldsToRp[] = $field;
          }
        }
      }
    }
    foreach ($fieldsToRp as $field) 
      $data['passparam']['tfoots'][$field] = "Rp ".number_format($data['passparam']['tfoots'][$field],2,',','.');

    $fields = $this->$model->fields();
    if (isset($get['mode']) && $get['mode'] == 'edit') {
      $record = $this->$model->findOne($get['id']);
      foreach ($fields as &$field) $field['value'] = $record[$field['name']];
    } else foreach ($fields as &$field) $field['value'] = '';
    $data['passparam']['fields'] = $fields;

    $this->loadview($data);
  }
}