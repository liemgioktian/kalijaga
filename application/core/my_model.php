<?php

Class my_model extends CI_Model {


  function __construct () {
    parent::__construct();
    $this->load->database();
  }

  function find ($where = array()) {
    foreach ($where as $field => $value) $this->db->where($field, $value);
    return $this->db->get($this->table)->result();
  }

  function theads () {
    return isset($this->theads) ? $this->theads : array();
  }

  function tfoots () {
    return isset($this->tfoots) ? $this->tfoots : array();
  }

  function filters () {
    return isset($this->filters) ? $this->filters : array();
  }

  function fields () {
    return isset($this->fields) ? $this->fields : array();
  }

  function save ($post, $id = null) {
    if (is_null($id)) $this->db->insert($this->table, $post);
    else $this->db->where('id', $id)->update($this->table, $post);
  }

  function findOne ($id) {
    return $this->db->get_where($this->table, array($this->table . '.id' => $id))->row_array();
  }

  function delete ($id) {
    $this->db->where('id', $id)->delete($this->table);
  }

  function tanggalbulan_to_bulantahun (&$where) {
    $timestamp = strtotime($where->tanggalbulan);
    $php_date = getdate($timestamp);
    $where->bulan = date("n", $timestamp);
    $where->tahun = date("Y", $timestamp);
    unset($where->tanggalbulan);
  }
}
