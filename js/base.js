$(function () {
  var wlh = window.location.href
  var wlhx = wlh.split('?')
  var mode = null
  if (wlhx[1]) mode = wlhx[1].split('&')[0].replace('mode=','')
  wlh = wlhx[0]
	var activemenu = $('.subnavbar ul li a[href="'+wlh+'"]')
	if (activemenu.length > 0) activemenu.parent('li').addClass('active')
	else $('.subnavbar ul li a').eq(0).parent('li').addClass('active')

  $('.subnavbar ul li.active').each (function () {
	  var title = $(this).find('a span').html()
	  $('.widget-header h3').html(title)
	  $('.tabbable ul.nav.nav-tabs li a').each (function () {
	    var ori = $(this).text()
	    $(this).text(ori + ' ' + title)
	  })
	})

  if (null !== mode && mode == 'edit') {
    $('.tab-pane').removeClass('active').last().addClass('active')
    $('ul.nav.nav-tabs li').removeClass('active').last().addClass('active').find('a').each (function () {
      var txt = $(this).text()
      $(this).text($(this).text().replace('Input', 'Edit'))
    })
  }

  $('form.model-filter button').unbind('click').bind('click', function (e) {
    e.preventDefault()
    var where = {}
    $('form.model-filter input, form.model-filter select').each(function () {
      if ($(this).val().length > 0) where[$(this).attr('name')] = $(this).val()
    })
    window.location = wlh + '?where=' + JSON.stringify(where)
  })

  $('.alert').removeClass('hidden').hide()
  $('a[href*="?mode=delete&id="]').each(function () {
    var href = $(this).attr('href')
    $(this).unbind('click').bind('click', function (e) {
      $('.alert')
        .slideDown()
        .find('.btn').each (function () {
          $(this).unbind('click').bind('click', function () {
            if ($(this).is('.ya')) window.location = href
            else if ($(this).is('.tidak')) $('.alert').slideUp()
          })
        })
      e.preventDefault()
    })
  })

  $("input[name$='_alias']").attr('disabled', true)

  if ($('input[name*="tanggal"]').length > 0)  {
    $('input[name*="tanggal"]').each (function () {
      var format = $(this).is('input[name="tanggalbulan"]') ? 'yyyy-mm' : 'yyyy-mm-dd'
      $(this).datepicker({
        format: format,
      }).on('changeDate', function(e){
        $(this).datepicker('hide');
      })
    })
  }

  if (window.location.href.indexOf('jatahjamaah') > -1) {
    $('table.table tbody tr').each(function () {
      var last = $(this).find('td').last()
      last.append('<a class="btn btn-small btn-primary btn-setor">setor</a>')
    })
    $('.btn-setor').click(function () {
      var siblings = $(this).siblings('.btn')
      var id = siblings.attr('href').split('id=')[1]
      $.get(site_url + '/proses/setor/' + id, function () {
        window.location.reload()
      })
    })
  }

});