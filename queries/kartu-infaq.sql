-- VARIABLE : JAMAAH.ID, BULAN,TAHUN
SELECT nama as item, '4/5/2019' as jatah
FROM jamaah WHERE id = 3

;-- UNION

-- SELECT '' as item, '' as jatah

-- UNION

SELECT b.* FROM (
  SELECT
  infaq.item as item,
  IF(dibulatkan = 0, '', CONCAT('Rp ', FORMAT(dibulatkan, 0))) as jatah

  FROM (`jatahjamaah`) 
  RIGHT JOIN `jamaah` ON `jatahjamaah`.`jamaah` = `jamaah`.`id` 
  JOIN `jatahdesa` ON `jatahjamaah`.`jatahdesa` = `jatahdesa`.`id` 
  JOIN `infaq` ON `jatahdesa`.`item` = `infaq`.`id` 
  JOIN `rumus` ON `jamaah`.`ring` = `rumus`.`ring` 

  WHERE `jamaah`.`id` = 3
  AND `bulan` = '5' AND `tahun` = '2019'
  AND (IF(infaq.item like '%NUANSA PERSONAL' and dibulatkan = 0, false, true))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%jimpitan%'))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%ORG/OR/DANSOS%'))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%ORGANISASI DAERAH%'))

  ORDER BY dibulatkan
) b

;-- UNION

SELECT * FROM (
SELECT 'SUBTOTAL' as item, CONCAT('Rp ', FORMAT(SUM(dibulatkan), 0)) as jatah
  FROM (`jatahjamaah`) 
  RIGHT JOIN `jamaah` ON `jatahjamaah`.`jamaah` = `jamaah`.`id` 
  JOIN `jatahdesa` ON `jatahjamaah`.`jatahdesa` = `jatahdesa`.`id` 
  JOIN `infaq` ON `jatahdesa`.`item` = `infaq`.`id` 
  JOIN `rumus` ON `jamaah`.`ring` = `rumus`.`ring` 

  WHERE `jamaah`.`id` = 3
  AND `bulan` = '5' AND `tahun` = '2019'
  AND (IF(infaq.item like '%NUANSA PERSONAL' and dibulatkan = 0, false, true))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%jimpitan%'))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%ORG/OR/DANSOS%'))
  AND (IF(jamaah.kk=1, true, infaq.item not like '%ORGANISASI DAERAH%'))
) x
UNION SELECT 'TOTAL' item, '' jatah