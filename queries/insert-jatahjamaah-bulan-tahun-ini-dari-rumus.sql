-- *** JANGAN LUPA GANTI BULAN DAN TAHUNNYA, SEMUA ***
-- BACKUP SEK, NGERI LHO NDES NEK NGASI SALAH, AJUR KOWE

-- KALAU ADA UPDATE JATAH DESA SETELAH BULAN INI DIGENERATE
DELETE FROM jatahjamaah WHERE jatahdesa IN (SELECT id FROM jatahdesa WHERE bulan = 5 and tahun = 2019);

-- INI QUERYNYA
INSERT INTO jatahjamaah (id, jamaah, jatahdesa, dijatah, dibulatkan, disetorkan)
SELECT
  '',
  jamaah.id,
  jatahdesa.id,
  CASE
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%JIMPITAN%' THEN 10000
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%ORG/OR/DANSOS 6RB/KK%' THEN 6000
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%ORGANISASI DAERAH%' THEN 10000
--     WHEN jamaah.id IN (7,8,9,10) AND infaq.item LIKE '%NUANSA%' THEN 12000
    ELSE rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal
  END,
  CASE
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%JIMPITAN%' THEN 10000
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%ORG/OR/DANSOS 6RB/KK%' THEN 6000
    WHEN jamaah.kk = 1 AND infaq.item LIKE '%ORGANISASI DAERAH%' THEN 10000
--     WHEN jamaah.id IN (7,8,9,10) AND infaq.item LIKE '%NUANSA%' THEN 12000
    ELSE round(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal / 1000, 0) * 1000
  END,
  0
FROM (`jamaah`)
JOIN `jatahdesa` ON 1 
JOIN `rumus` ON `jamaah`.`ring` = `rumus`.`ring`
JOIN infaq ON jatahdesa.item = infaq.id
WHERE jatahdesa.bulan = 5 and jatahdesa.tahun = 2019 and jamaah.ring > 0;