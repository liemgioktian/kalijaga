
SELECT
	jamaah.nama
	, infaq.item
	, IF(0 = jatahjamaah.dijatah, jatahjamaah.disetorkan, jatahjamaah.dijatah)
FROM
	jatahjamaah
LEFT JOIN jamaah ON jamaah.id = jatahjamaah.jamaah
LEFT JOIN jatahdesa ON jatahjamaah.jatahdesa = jatahdesa.id
LEFT JOIN infaq ON jatahdesa.item = infaq.id

WHERE infaq.id = 1

GROUP BY jamaah.id
ORDER BY jamaah.nama