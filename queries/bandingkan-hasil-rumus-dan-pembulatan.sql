SELECT 
sum(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal) as dihitung,
sum(round(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal / 1000, 0) * 1000) as dibulatkan,
sum(round(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal / 1000, 0) * 1000) -
sum(rumus.prosentase / 100 / (SELECT count(jamaah.id) FROM jamaah WHERE ring = rumus.ring) * jatahdesa.nominal) as sisanya
FROM (`jatahjamaah`) RIGHT JOIN `jamaah` ON `jatahjamaah`.`jamaah` = `jatahjamaah`.`id` JOIN `jatahdesa` ON 1 JOIN `infaq` ON `jatahdesa`.`item` = `infaq`.`id` JOIN `rumus` ON `jamaah`.`ring` = `rumus`.`ring`;