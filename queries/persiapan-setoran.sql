

  SELECT 
  jd.id, i.level, i.item /* YANG INI BUAT BIKIN URUTAN ID QUERY DIBAWAHNYA */
--   group_concat(i.item) /* YANG INI BUAT BIKIN HEADER TABLE DI EXCEL, caranya di copas k text editor, save as csv, buka pake excel */
  FROM jatahdesa jd JOIN infaq i ON jd.item = i.id
  order by jd.id
;


-- MASUKIN RECORDNYA DARI SINI
  SELECT 
    j.nama, 
-- PUSAT
    SUM( IF( jatahdesa = 36, disetorkan, 0 ) ) AS Q,
    SUM( IF( jatahdesa = 1, disetorkan, 0 ) ) AS A,
    SUM( IF( jatahdesa = 2, disetorkan, 0 ) ) AS B,
    SUM( IF( jatahdesa = 3, disetorkan, 0 ) ) AS C,
    SUM( IF( jatahdesa = 34, disetorkan, 0 ) ) AS O,
-- DAERAH
    SUM( IF( jatahdesa = 35, disetorkan, 0 ) ) AS P,    
    SUM( IF( jatahdesa = 4, disetorkan, 0 ) ) AS D,
    SUM( IF( jatahdesa = 5, disetorkan, 0 ) ) AS E,
    SUM( IF( jatahdesa = 6, disetorkan, 0 ) ) AS F,
    SUM( IF( jatahdesa = 7, disetorkan, 0 ) ) AS G,
    SUM( IF( jatahdesa = 8, disetorkan, 0 ) ) AS H,
-- DESA
    SUM( IF( jatahdesa = 9, disetorkan, 0 ) ) AS I,
-- KELOMPOK
    SUM( IF( jatahdesa = 10, disetorkan, 0 ) ) AS L,
    SUM( IF( jatahdesa = 12, disetorkan, 0 ) ) AS M,
    SUM( IF( jatahdesa = 33, disetorkan, 0 ) ) AS N
  FROM jatahjamaah jj
  JOIN jamaah j ON j.id = jj.jamaah
  JOIN jatahdesa jd ON jj.jatahdesa = jd.id
  JOIN infaq i ON jd.item = i.id
  GROUP BY j.id